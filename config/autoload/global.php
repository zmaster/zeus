<?php
return [
    'zf-content-negotiation' => [
        'selectors' => [],
    ],
    'db' => [
        'adapters' => [
            'dummy' => [],
            'postgres' => [],
        ],
    ],
    'router' => [
        'routes' => [
            'oauth' => [
                'options' => [
                    'spec' => '%oauth%',
                    'regex' => '(?P<oauth>(/autenticate))',
                ],
                'type' => 'regex',
            ],
        ],
    ],
    'zf-mvc-auth' => [
        'authentication' => [
            'adapters' => [
                'autentication' => [
                    'adapter' => \ZF\MvcAuth\Authentication\OAuth2Adapter::class,
                    'storage' => [
                        'adapter' => \pdo::class,
                        'dsn' => 'pgsql:host=127.0.0.1;port=5432;dbname=db_zeus;',
                        'route' => '/autenticate',
                        'username' => 'postgres',
                        'password' => 'qazwsx123456',
                    ],
                ],
            ],
            'map' => [
                'Corporate\\V1' => 'autentication',
                'Oversight\\V1' => 'autentication',
                'Relationships\\V1' => 'autentication',
            ],
        ],
    ],
    'doctrine' => [
        'orm_migration' => [
            'driverClass' => \Doctrine\DBAL\Driver\PDOPgSql\Driver::class,
            'params' => [
                'port' => '5432',
                'host' => '127.0.0.1',
                'user' => 'postgres',
                'password' => 'qazwsx123456',
                'dbname' => 'db_zeus',
                'schema' => 'public',
                'charset' => 'utf8',
            ],
        ],
    ],
];
